---
title: One year, one list
description: Things I didn't know
date: 2022-09-30
slug: post-seis
image: arcoiris1.jpg
categories:
    - Posts
---

# Some of the things I have learned after one year in Leeuwarden

So many things. 

Maybe some of them I am rediscovering, some of them I didn't see in the past. 

This is just a selection of a few.

1. Escher (one of my favourite artists) was from Leeuwarden.

2. Dutch painters have been inspired by trees in a profound way. Just one of those paintings from a very common tree in Leeuwarden (and many cities in NL): Van Gogh and the [horse chestnut trees](https://www.vangoghmuseum.nl/en/collection/s0126V1962)

3. The "hearts" in the Friesland flag are actually _pompebleden_ or yellow water lillies.

4. Friesland is the home of amazing people with a history of hard work and a empowering sense of community. 

5. There is a London plane ( _Platanus hispanica_) in front of my house! These trees can live up to 500 hundred years and are very common in cities.

6. The Mata Hari was from Leeuwarden. 

7. Winter flowers.

8. Wadden sea.

9. [Fryslan Nature museum](https://natuurmuseumfryslan.nl/en/)

10. Amazing sunsets and beautiful rainbows






