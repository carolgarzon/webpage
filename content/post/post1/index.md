---
title: Research interests (how they came about?)
description: Why spatial ecology?
date: 2023-03-09
slug: post-uno
image: fern.jpg
categories:
    - Posts
---

## Why spatial ecology?

Location, location, location! The world is all about where things are and how they interact with each other, from a drop of water on a leaf to the flying rivers of the Amazon. All of us interact with the plants and animals around us and the temperature, wind, and rainy patterns in our towns and cities. It is something we don't think about a lot, but that has determined the clothes we are wearing, the food we like to eat, the music we love and the language we speak. Our culture and traditions.


I have been primarily an observer of those connections. Since I was little, I used to be intrigued by the songs my mom and my dad used to sing, a mixture of sounds and rhythms. The music my mom heard was full of guitars, pianos, and trumpets. My dads' music, fast and festive, full of percussion and clarinets. Boleros, bambucos, tangos, vallenatos and cumbias. I used to wonder where those sounds come from? Why do they mention those plants and animals? What was the authors' inspiration?. At that time, I did not know his/her inspiration was based on his/her spatial and temporal context. And that the rhythm and the clothes related to the music had a lot to do with the climate.


Later, when I was a master's student in the Netherlands, I met people from other countries like South Africa, Romania, Italy, Serbia, France, and the list goes on… from them; I learned about other cultures and traditions. I found it fascinating how different and similar we were. At the same time, I was learning more and more about the ecology of European and African ecosystems and how species distributed, interacting, and modifying the landscape to compete and share resources. I learned about maps and spatial analysis, the fascinating set of tools that allow us to explore ecological processes at multiple scales and including the socio-economic context of the population living in those areas. I became interested in all things maps and images, software and satellite, airplanes, multispectral and hyperspectral images.


I am still an observer of how nature and society interact. I like traveling, learning about history, geography, and music, and I love exploring the world through the eyes of remote sensing and combining the data they provide with field experiments.


Let me share three of my favorite songs from that time, one from my dad's collection. If you click on the photo, it will link you to the music on YouTube.

**El mochuelo - Otto Serge y Rafael Ricardo (Vallenato)**

[![](http://img.youtube.com/vi/rmo9c40A_w0/0.jpg)](https://youtu.be/olFQ3l8VNlw?si=UfzsaP4JADWkL054 "El mochuelo")

_Mochuelo is the common name of a bird from Colombia. [Here](https://ebird.org/species/grysee1?siteLanguage=es#) you can see it and hear his song. And below is the location of los Montes de Maria_

https://goo.gl/maps/QnnfrnEUkkDihSJh9

And one from my mom collection:

**Las acacias - Jose Molina Cano  (Pasillo)**

[![](http://img.youtube.com/vi/N4OU0_hhz8w/0.jpg)](http://www.youtube.com/watch?v=N4OU0_hhz8w "Las acacias")

The author was from Medellin, a city in the middle of the Andes. That's Antioquia, a region full of countryside houses that resemble that song.

https://goo.gl/maps/7NBPd6meCgQax4q58

And one that both of them used to hear, and that is the hymn of el Meta, the region of tropical savannas.

**Ay mi llanura - Arnulfo Briceño (Joropo)**

[![](http://img.youtube.com/vi/ISkad65f5fU/0.jpg)](http://www.youtube.com/watch?v=ISkad65f5fU "Ay mi llanura")

And in this song, there are so many references to the biodiversity of the tropical savanna that it is impossible to name all. I will just add the location in case you may want to visit it! 

https://goo.gl/maps/bqsP3npPKJHAPhX79
