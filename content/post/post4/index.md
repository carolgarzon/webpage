---
title: Publications
description: The cool, confusing and (not so) complicated stuff ;)
date: 2024-11-05
slug: post-cuatro
image: libro.JPG
categories:
    - Posts
---

# Scientific papers 

Garzon-Lopez, C.X, Andreo, V., Bellis, L., Castillo, C., Isacs, L., Lasso, E., Nadim, T., Rocchini, D., Van Meerbeek, K., Beaulieu, A. Cancion con todos. A biodiversity monitoring framework for transformative change. _Submitted to People & Nature_ 

Garzon-Lopez, C. X., Miranda, A., Moya, D., & Andreo, V. 2024. [Remote sensing biodiversity monitoring in Latin America: Emerging need for sustained local research and regional collaboration to achieve global goals](https://doi.org/10.1111/geb.13804) Global Ecology and Biogeography, 33, e13804. 

Garzon Lopez, C.X., and Savickytė, G. 2023. [Biodiversity in cities: the impact of biodiversity data across spatial scales on diversity estimates](https://doi.org/10.2478/foecol-2023-0012) Folia Oecologica, vol. 50, no. 2, pp. 134-146. 

Rocchini, D., Tordoni, E., Marchetto, E. et al. 2023 [A quixotic view of spatial bias in modelling the distribution of species and their diversity](https://doi.org/10.1038/s44185-023-00014-6) npj biodivers 2, 10.

Ayarza-Páez, A., Garzon-Lopez, C. X., & Lasso, E. 2022. [Habitat preference and vulnerability to drought of three Hypericum species of the páramo](https://doi.org/10.1080/17550874.2022.2143731) Plant Ecology & Diversity, 15(5–6), 281–295. 

Vargas, C.A., Bottin, M., Särkinen, T., Richardson, J.E., Raz, L., Garzon-Lopez, C.X., Sanchez, A. 2022 [Environmental and geographical biases in plant specimen data from the Colombian Andes](https://doi.org/10.1093/botlinnean/boac035). Botanical Journal of the Linnean Society, Volume 200, Issue 4. Pages 451–464.

Garzon-Lopez, C. X. , Barcenas, E. M. , Ordoñez, A. , Jansen, P. A. , Bohlman, S. A. , & Olff, H. 2022. [Recruitment limitation in three large-seeded plant species in a tropical moist forest](https://doi.org/10.1111/btp.13063) Biotropica, 54, 418–430.

Lasso, E., Matheus, P., Gallery, R., Garzón-López, C.X., Cruz, M., León, I., Ayarza, A., Aragón, L. & Curiel, J. [Homeostatic response to experimental warming suggests high intrinsic natural resistance in the Paramos to warming. 2021. Frontiers in Ecology and Evolution](https://doi.org/10.3389/fevo.2021.615006) 9, 55.

Rocchini, D., Salvatori, N., Beierkuhnlein, C., Chiarucci, A., de Boissieu, F., Förster, M., Garzon-Lopez, C.X., Gillespie, T.W., Hauffe, H.C., He, K.C., Kleinschmit, B., Lenoir, J., Malavasi, M., Moudry, V., Nagendra, H., Payne, D., Simova, P., Torresani, M., Wegmann, M., Feret, J-B. 2021. [From local spectral species to global spectral communities: a benchmark for ecosystem diversity estimate by remote sensing](https://www.sciencedirect.com/science/article/pii/S157495412030145X). Ecological Informatics 61 (2021) 101195.

Garzon-Lopez, C.X., Lasso, Eloisa. 2020. [Species classification in a tropical alpine ecosystem (Paramo) using UAV-borne optical and hyperspectral imagery](https://www.mdpi.com/2504-446X/4/4/69/htm). Special issue SheMaps. Drones 4 (4) 69.

Zambrano, J., Cordeiro, N.J., Garzon-Lopez, C.X. Yeager, L., Fortunel, C., Ndangalasi, H.j. & Beckman, N. 2020. [Investigating the direct and indirect effects of forest fragmentation on plant functional diversity](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0235210). PLoS ONE 15(7): e0235210.

Fevola, C., Rossi, C., Rosso, F., Girardi, M., Rosà, R., Manica, M., Delucchi, L., Rocchini, D., Garzon-Lopez, C.X., Arnoldi, D., Bianchi, A., Buzan, E., Charbonnel, N., Collini, M., Ďureje, L., Ecke, F., Ferrari, N., Fischer, S., Gillingham, E., Hörnfeldt, B., Kazimírová, M., Konečný, A., Maas, M., Magnusson, M., Miller, A., Niemimaa, J., Nordström, A., Obiegala, A., Olsson, G., Pedrini, P., Piálek, J., Reusken, C., Rizzolli, F., Romeo, C., Silaghi, C., Sironen, T., Stanko, M., Tagliapietra, V., Ulrich, R., Vapalahti, O., Voutilainen, L., Wauters, L., Rizzoli, A., Vaheri, A., Jääskeläinen, A., Henttonen, H. &Heidi C. Hauffe. 2020. [Geographical Distribution of Ljungan Virus in Small Mammals in Europe](https://www.liebertpub.com/doi/10.1089/vbz.2019.2542). Vector-Borne and Zoonotic Diseases

Ewald, M., Skowronek, S., Aerts, R., Lenoir, J., Feilhauer, H., van der Kerchove, R., Honnay, O., Somers, B., Garzon-Lopez, C.X., Rocchini, D. & Schmidtlein, S. 2020. [Assessing the impact of an invasive moss using high resolution imaging spectroscopy](https://www.sciencedirect.com/science/article/abs/pii/S1470160X19308775). Ecological Indicators 110. 105882

Almoussawi, A., Lenoir, J., Jamoneau, A., Hattab, T., Safaa, W., Gallet-Moron, E., Garzon-Lopez, C.X., Spicher, F., Kobaissi, A. & Decocq, G. 2020. [Forest fragmentation shapes the alpha-gamma relationship in plant diversity](https://onlinelibrary.wiley.com/doi/abs/10.1111/jvs.12817). Journal of Vegetation Science 31 (1) 63-74.

Zambrano, J., Garzon-Lopez, C.X. Yeager, L., Fortunel, C., Cordeiro, N.J. & Beckman, N. 2019. [The effects of habitat loss and fragmentation on plant functional traits and functional diversity: what do we know so far?](https://link.springer.com/article/10.1007/s00442-019-04505-x). Oecologia 191 (3) 505-518.

Skowronek, S. Van De Kerchove, R., Rombouts, B., Aerts, R., Ewald, M., Warrie, J., Schiefer, F., Garzon-Lopez, C.X., Hattab, T., Honnay, O., Lenoir, J., ROcchini, D., Schmidtlein, S., Somers,B. & Feilhauer, H. 2018. [Transferability of species distribution models for the detection of an invasive alien bryophyte using imaging spectroscopy data](https://www.sciencedirect.com/science/article/abs/pii/S0303243418301132). International Journal of Applied Earth Observation and Geoinformation 68, 61-72.

Ewald, M., Aerts, R., Lenoir, J., Fassnacht, F.E., Nicolas, M., Skowronek, S., Piat, J., Honnay, O., Garzon-Lopez, C.X., Feilhauer, H., Van De Kerchove, R., Somers, B., Hattab, T., Rocchini, D., Schmidtlein, S. 2018. [LiDAR derived forest structure data improves predictions of canopy N and P concentrations from imaging spectroscopy](https://www.sciencedirect.com/science/article/abs/pii/S0034425718301391). Remote Sensing of Environment 211, 13-25.

Garzon-Lopez, C.X., Hattab, T., Skowronek, S., Aerts, R., Ewald, M., Feilhauer, H., Honnay, O., Decocq, G., Van De Kerchove, R., Somers, B., Schmidtlein, S., Rocchini, D., Lenoir, J. 2018. [The DIARS toolbox: a spatially explicit approach to monitor alien plant invasions through remote sensing](https://riojournal.com/article/25301/). Research Ideas and Outcomes 4: e25301

Rocchini, D., Bacaro, G., Chirici, G. Da Re, D., Feilhauer, H., Foody, G.M., Galluzzi, M., Garzon-Lopez, C.X., … Wegmann, M., Rugani, B. 2018. [Remotely sensed spatial heterogeneity as an exploratory tool for taxonomic and functional diversity study](https://www.sciencedirect.com/science/article/abs/pii/S1470160X17306234). Ecological Indicators 85, 983-990.

Ewald, M., Skowronek, S., Aerts, R., … Garzon-Lopez, C.X., Decocq, G., Van De Kerchove, R., Somers, B., Rocchini, D., Schmidtlein, S. 2018. [Analyzing remotely sensed structural and chemical canopy traits of a forest invaded by Prunus serotina over multiple spatial scales](https://link.springer.com/article/10.1007/s10530-018-1700-9). Biological Invasions 20, 2257.

Hattab T, Garzon-Lopez CX, Ewald M, et al. 2017. [A unified framework to model the potential and realized distributions of invasive species within the invaded range](https://onlinelibrary.wiley.com/doi/full/10.1111/ddi.12566). Diversity and Distributions 00, 1–14.

Rocchini, D., Garzon-Lopez, C.X., Marcantonio, M., Amici, V., Bacaro, G., Bastin, L., Brummitt, N., Chiarucci, A., Foody, G.L., Hauffe, H.C., He, K.S., Ricotta, C., Rizzoli, A., Rosà, R. 2017. [Anticipating species distributions: Handling sampling effort bias under a Bayesian framework](https://www.sciencedirect.com/science/article/abs/pii/S0048969716327231). Science of The Total Environment 584–585, 282-290.

Aerts, R., Ewald, M., Nicolas, M., Piat, J., Skowronek, S., Lenoir, J., Hattab, T., Garzon-Lopez, C.X., Feilhauer, H., Schimidtlein, S., Rocchini, D., Decocq, G., Somers, B., Van de Kerchove, R., Denef, K., Honnay, O. 2017. [Invasion by the Alien Tree Prunus serotina Alters Ecosystem Functions in a Temperate Deciduous Forest](https://www.frontiersin.org/articles/10.3389/fpls.2017.00179/full). Frontiers in Plant Science, 8, 179.

Foody, G.M.; Pal, M.; Rocchini, D.; Garzon-Lopez, C.X.; Bastin, L. 2016. [The sensitivity of mapping methods to reference data quality: training supervised image classifications with imperfect reference data](https://www.mdpi.com/2220-9964/5/11/199). ISPRS International Journal of Geo-Information 5, 199.

Garzon-Lopez C.X, Bastin, L., Foody G.M., Rocchini, D. 2016. [A virtual species set for robust and reproducible species distribution modelling tests](https://www.sciencedirect.com/science/article/pii/S2352340916300804). Data in brief, 7, 476-479.

Garzon-Lopez C.X., Ballesteros-Mejia, L., Bohlman, S.A., Ordoñez, A., Jansen, P.A., & Olff, H. 2015. [Indirect interactions among tropical tree species through shared rodent seed predators: a novel mechanism of tree species coexistence](https://onlinelibrary.wiley.com/doi/abs/10.1111/ele.12452). Ecology letters, 18, 752-760.

Marcantonio, M., Pareeth, S., Rocchini, D., Metz, M., Garzon-Lopez, C.X., Neteler, M. 2015. [The integration of artificial night-time lights in landscape ecology: a remote sensing approach](https://www.sciencedirect.com/science/article/abs/pii/S1476945X15000318). Ecological complexity, 22, 109-120.

Rocchini, D., Andreo, V., Förster, M., Garzon-Lopez, C.X., Gutierrez, A.P., Gillespie, T.W., Hauffe, H.C., He, K., Kleinschmit, B., Mairota, P., Marcantonio, M., Metz, M., Nagendra, H., Pareeth, S., Ponti, L., Ricotta, C., Rizzoli, A., Schaab, G., Zebisch, M., Zorer, R. & Neteler, M. 2015 [Potential of remote sensing to predict species invasions: A modelling perspective](https://journals.sagepub.com/doi/abs/10.1177/0309133315574659).  Progress in Physical geography, 39, 283-309.

Rocchini, D., Comber, A., Garzon-Lopez, C.X., Neteler, M., Barbosa, A.M., Marcantonio, M., Groom, Q., da Costa Fonte, C. & Foody, G.M. 2015. [Explicitly accounting for uncertainty in crowdsourced data for species distribution modelling](https://www.isprs-ann-photogramm-remote-sens-spatial-inf-sci.net/II-3-W5/333/2015/). ISPRS Annals of the photogrammetry, remote sensing and spatial information sciences 3, 333-337.

Brown, D.D., Montgomery, R.A., Millspaugh, J.J., Jansen, P.A., Garzon-Lopez, C.X. & Kays, R. 2014. [Selection and spatial arrangement of rest sites within northern tamandua home ranges](https://zslpublications.onlinelibrary.wiley.com/doi/10.1111/jzo.12131). Journal of Zoology 293, 160-170.

Garzon-Lopez C.X., Jansen, P.A., Bohlman, S.A., Ordoñez, A. & Olff, H. 2014. [Effects of sampling scale on patterns of habitat association in tropical trees](https://onlinelibrary.wiley.com/doi/abs/10.1111/jvs.12090). Journal of Vegetation Science 25, 349-362.

Garzon-Lopez C.X., Bohlman, S.A., Olff, H. & Jansen, P.A. 2013. [Mapping tropical forest trees using high-resolution aerial digital photographs](https://onlinelibrary.wiley.com/doi/abs/10.1111/btp.12009). Biotropica, 45, 308-316.

Caillaud, D., Crofoot, M.C., Scarpino, S.V., Jansen, P.A., Garzon-Lopez, C.X., Winkelhagen, S.A., Bohlman, S.A. & Walsh, P.D. 2010. [Modeling the spatial distribution and fruiting pattern of a key tree species in a Neotropical forest: methodology and potential applications](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0015002). PloS ONE 5, 11.

Jansen, P.A., Bohlman, S.A., Garzon-Lopez, C.X., Olff, H., Muller-Landau, H.C. & Wright, S.J. 2008. [Large scale spatial variation in palm fruit abundance across a tropical moist forest estimated from high-resolution aerial photographs](https://onlinelibrary.wiley.com/doi/full/10.1111/j.2007.0906-7590.05151.x). Ecography 31: 33-42.

# Other publications, book chapters and scientific reports

Rocchini, D., Garzon-Lopez, C.X., Barbosa, A.M., Delucchi, L., Olandi, J.E., Marcantonio, M., Bastin, L., Wegmann, M. 2018. [GIS-Based Data Synthesis and Visualization](https://link.springer.com/chapter/10.1007/978-3-319-59928-1_13). In: Ecknagel, F., Michener, W. (eds) Ecological Informatics. Springer, Cham.

Garzon-Lopez, C.X, Hattab, T., Ewald, M., Skowronek, S., Aerts, R., Horen, H., Brasseur, B., Gallet-Moron, E. Spicher, F. Decocq, G., Feilhauer H., Honnay, O., Kempeneers, P., Schmidtlein, S., Somers, B., Van De Kerchove, R., Rocchini, D. & Lenoir, J. 2016. DIARS toolbox. DIARS project [Detection of Invasive plant species and Assessment of their impact on ecosystem properties through Remote Sensing](http://diarsproject.github.io/DIARS/HomeDIARS.html)

Garzon-Lopez, C.X. and Frame, D. 2016. [Applications of remote sensing to Tropical forest restoration: Challenges and opportunities](https://www.forru.org/library/0000099). In Elliot S. (Ed), Automated forest restoration.

Garzon-Lopez, C.X. 2015. Infographic Species distribution models. European Union Biodiversity Observation Network [EU BON](http://beta.eubon.ebd.csic.es/infograph-species-distribution-models).

Rocchini, D. & Garzon-Lopez C.X. 2015. Cartograms tool to represent spatial uncertainty in species distribution. [Decision support tool](http://beta.eubon.ebd.csic.es)

Rocchini, D., Marcantonio, M., Foody, G.M., Garzon Lopez, C.X., He, K.S., Kühn, I., Metz, M., Neteler, M.G., Turner, W., Hortal, J. 2015. Uncertainty surfaces and maps of ignorance: the possibility of spatially estimating dark diversity. In: 58th IAVS Symposium: Understanding broad-scale vegetation patterns, 19 – 24 July 2015, Brno, Czech Republic.
