---
title: Ecologia espacial - Curso
description: GRASS GIS y R
date: 2021-02-02
slug: post-tres
image: bosque.jpg
categories:
    - Posts
---

There are a number of Free and Open Source Softwares available for spatial analyses. One of the most powerful ones is GRASS GIS which combined with R provides a wide range of tools for image processing and data analysis in ecology (among other applications). In this course (in Spanish), we covered the basics of GRASS GIS and learned some of the applications of GRASS GIS and R in Ecology:

# [**Curso de ecología espacial**](https://carolgarzon.gitlab.io/ecologiaespacial/courses/)
