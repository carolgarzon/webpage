---
title: Talking about drones...
description: Paramos from the sky!
date: 2021-02-01
slug: post-dos
image: paramito.jpg
categories:
    - Posts
---

I was invited to [**Ciencia cafe pa sumerce**](https://cienciacafesumerce.wordpress.com/), an effort to disseminate science in Colombia and Latinamerica. We talked about drones, species identification, paramo conservation and citizen science...

[![](http://img.youtube.com/vi/DHdUWbT-8RI/0.jpg)](http://www.youtube.com/watch?v=DHdUWbT-8RI "¿Cómo estudiar el páramo con drones?")
