---
title: About
description: Carol X Garzon
date: '2019-02-28'
aliases:
  - about-us
  - about-hugo
  - contact
license: CC BY-NC-ND
lastmod: '2023-10-09'
menu:
    main:
        weight: -90
        pre: user
---

I am a Colombian researcher and the director of [Verde Elemental](http://www.verde-elemental.org/), a website in Spanish, dedicated to promoting and disseminating knowledge in ecology and conservation for Latin America.  Currently, I am working as an Assistant Professor in Earth and Environmental Sciences  in the Knowledge Infrastructures department at the Faculty Campus Fryslan in the University of Groningen (The Netherlands). Previous to this job, I worked in the plant ecophysiology lab (ECOFIV) at Universidad de los Andes (Bogotá, Colombia). During 2014-2016 I worked as a postdoctoral researcher at Fondazione Edmund Mach (Italy), where I provided expertise on the use of remote sensing tools and integrative approaches for species distribution modeling as part of the European Union Biodiversity Observation Network project (EU BON). And during 2016, I was part of the DIARS project where we performed research studies and developed a set of tools to detect, map and monitor invasive plant species through remote sensing. During my time in Italy I became interested in Free and Open Source Software (specifically in GRASS GIS) and since I have taught workshops and develop tools using FOSS software.

I am interested in how socioecological dynamics influence biodiversity patterns and processes across spatiotemporal scales in a changing world, with particular emphasis on how this shapes our knowledge on the state and maintenance of biodiversity. Methodologically, I am interested in the use of spatial tools for research on biodiversity monitoring and conservation.  I hold a Master's degree and Ph.D. from Groningen University, Netherlands.  During my Ph.D. I studied the determinants of the spatial distribution of tree species in a tropical forest in Panama, at the Smithsonian Tropical Research Institute, where I have also collaborated in education and public communication.

Contact email: c.x.garzon@gmail.com
